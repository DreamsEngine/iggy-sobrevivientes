<?php

if (!function_exists('body_data')) :
    function body_data($data)
    {
        global $post;

        $postId = '';
        $catArray = '';
        $tagArray = '';
        $home= '';
        $catName= '';
        $tagName= '';
        $author= '';

        if (is_home() || is_front_page()) {
            $postId = $post->ID;
            $catName = 'home';

        }

        if (is_single() || is_singular()) {
            $postId = $post->ID;
            $category = get_the_terms($post->ID, 'category');

            if ($category || is_wp_error($category)) {
                $catName = $category[0]->name;
            }

            $author_id = get_post_field ('post_author', $post->ID);
            $author_name = get_the_author_meta( 'display_name' , $author_id );
            
            if ($author_id || is_wp_error($author_id)) {
                $author = $author_name;

            }

        }
        
        if (is_category()) {
            $category = get_queried_object();
            $catName = $category->name;
        }

        if (is_category() || is_single()) {
            $catArray = [];
            $categories = get_the_category();

            

            foreach ($categories as $cat) {
                $catId = $cat->cat_ID;
                array_push($catArray, $catId);
            }
        }

        if (is_tag() || is_single()) {
            $tagArray = [];
            $tags = get_the_tags();
            $tags = $tags == false ? [] : $tags;
            foreach ($tags as $tagIds) {
                $tagId = $tagIds->term_id;
                array_push($tagArray, $tagId);
            }
        }





        $data  = array();
        $data["@context"] = array(
            'postId' => $postId == null ? '' : "$postId",
            'catId' => $catArray == null ? '' : implode( ", ", $catArray),
            'tagId' => $tagArray == null ? '' : implode( ", ", $tagArray),
            'typeId' => '',
            'taxId' => '',
            'dataSection' => array(
                'catName' => $catName == null ? '' : "$catName",
                'author' => $author == null ? '' : "$author",

            ),
            
        );
        
        echo "\n<script>dfp = " . json_encode($data) . "</script>\n";
    }
endif;

add_action('wp_head', 'body_data', 999);
