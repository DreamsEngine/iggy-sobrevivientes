<?php
class DREAMS_AMP_Ad_Injection_Sanitizer extends AMP_Base_Sanitizer
{
    public function sanitize()
    {
        $body = $this->get_body_node();  
      
        // Build our amp-ad tag
        $first_node = AMP_DOM_Utils::create_node($this->dom, 'amp-ad', array(
            // Taken from example at https://github.com/ampproject/amphtml/blob/master/builtins/amp-ad.md
            'width' => 320,
            'height' => 250,
            'type' => 'doubleclick',
	        'data-multi-size' => '300x250,320x250',
	        'data-slot' => '/21825384248/mm-default-box-a'
        ));

        $second_node = AMP_DOM_Utils::create_node($this->dom, 'amp-ad', array(
            // Taken from example at https://github.com/ampproject/amphtml/blob/master/builtins/amp-ad.md
            'width' => 320,
            'height' => 250,
            'type' => 'doubleclick',
	        'data-multi-size' => '300x250,320x250',
	        'data-slot' => '/21825384248/mm-default-box-b'
        ));

        $third_node = AMP_DOM_Utils::create_node($this->dom, 'amp-ad', array(
            // Taken from example at https://github.com/ampproject/amphtml/blob/master/builtins/amp-ad.md
            'width' => 320,
            'height' => 250,
            'type' => 'doubleclick',
	        'data-multi-size' => '300x250,320x250',
	        'data-slot' => '/21825384248/mm-default-box-c'
        ));

        // Add a placeholder to show while loading
        $fallback_node = AMP_DOM_Utils::create_node($this->dom, 'amp-img', array(
            'placeholder' => '',
            'layout' => 'fill',
            'src' => 'https://miro.medium.com/max/1080/0*DqHGYPBA-ANwsma2.gif',
	        'width' => '90px',
            'height' => '90px',
        ));

        $first_node->appendChild($fallback_node);
        $second_node->appendChild($fallback_node);
        $third_node->appendChild($fallback_node);

        // If we have a lot of paragraphs, insert before the 4th one.
        // Otherwise, add it to the end.
        $p_nodes = $body->getElementsByTagName('p');
   
        if ($p_nodes->length > 0) {
            $p_nodes->item(0)->parentNode->insertBefore($first_node, $p_nodes->item(0));
        } else {
            $body->appendChild($first_node);
        }

        if ($p_nodes->length > 4) {
            $p_nodes->item(4)->parentNode->insertBefore($second_node, $p_nodes->item(4));
        } else {
            $body->appendChild($second_node);
        }

        if ($p_nodes->length > 8) {
            $p_nodes->item(8)->parentNode->insertBefore($second_node, $p_nodes->item(8));
        } else {
            $body->appendChild($second_node);
        }
    }
}