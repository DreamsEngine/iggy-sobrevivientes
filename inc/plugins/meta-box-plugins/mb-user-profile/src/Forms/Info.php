<?php
namespace MBUP\Forms;

class Info extends Base {
	protected function has_privilege() {
		if ( ! is_user_logged_in() ) {
			esc_html_e( 'Please login to continue.', 'mb-user-profile' );
			return false;
		}
		return true;
	}

	protected function submit_button() {
		?>
		<div class="rwmb-field rwmb-button-wrapper rwmb-form-submit">
			<button class="rwmb-button" id="<?php echo esc_attr( $this->config['id_submit'] ); ?>" name="rwmb_profile_submit_info" value="1"><?php echo esc_html( $this->config['label_submit'] ); ?></button>
		</div>
		<?php
	}
}
