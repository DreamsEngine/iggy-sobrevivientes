<?php
function pinno_news_child_enqueue_styles() {

    $parent_style = 'pinno-custom-style';

    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'fontawesome-child', get_stylesheet_directory_uri() . '/font-awesome/css/font-awesome.css' );
    wp_enqueue_style( 'pinno-custom-child-style',
        get_stylesheet_directory_uri() . '/style.css', 
        array( $parent_style ),
        wp_get_theme()->get('Version')
    );
    

    wp_enqueue_style(
        'dreams-style',
        get_stylesheet_directory_uri() . '/assets/app.css',
        false,
        date('ymdHis', filemtime(get_stylesheet_directory() . '/assets/app.css'))
      );
  

    wp_enqueue_script(
        'dreams-js',
        get_stylesheet_directory_uri() . '/assets/app.js',
        array(),
        date('ymdHis', filemtime(get_stylesheet_directory() . '/assets/app.js')),
        true
      ); 
    
}
add_action( 'wp_enqueue_scripts', 'pinno_news_child_enqueue_styles' );

