<?php 
if ( ! function_exists( 'dreams_setup' ) ) :

    function dreams_setup() {
  
        load_theme_textdomain('iggy-type-0',get_stylesheet_directory() . '/languages');

        $locale = get_locale();
        $locale_file = get_stylesheet_directory(). "/languages/$locale.php";
        if ( is_readable( $locale_file ) )
        require_once( $locale_file );
        
      // launching operation cleanup
      add_action('init', 'dreams_head_cleanup');
  
      //Metaboxes
      add_filter('rwmb_meta_boxes', 'den_register_meta_boxes');
  
    }
    endif;

    add_action( 'after_setup_theme', 'dreams_setup', 99 );

    if (!function_exists('dreams_head_cleanup')) :
  
        function dreams_head_cleanup()
        {
            // category feeds
            remove_action('wp_head', 'feed_links_extra', 3);
            // post and comment feeds
            remove_action('wp_head', 'feed_links', 2);
            // EditURI link
            remove_action('wp_head', 'rsd_link');
            // windows live writer
            remove_action('wp_head', 'wlwmanifest_link');
            // index link
            remove_action('wp_head', 'index_rel_link');
            // previous link
            remove_action('wp_head', 'parent_post_rel_link', 10, 0);
            // start link
            remove_action('wp_head', 'start_post_rel_link', 10, 0);
            // links for adjacent posts
            remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
            // WP version
            remove_action('wp_head', 'wp_generator');
    
            //change read more
            add_filter('the_content_more_link', 'modify_read_more_link');
            
            add_filter('show_admin_bar', '__return_false');
  
            // all actions related to emojis
            remove_action( 'admin_print_styles', 'print_emoji_styles' );
            remove_action('wp_head', 'print_emoji_detection_script', 7);
            remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
            remove_action('wp_print_styles', 'print_emoji_styles');
            remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
            remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
            remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
    
    
        } /* end dreams head cleanup */
    endif;
  