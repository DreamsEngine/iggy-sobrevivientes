<?php
  /*
    Plugin Name: Home Red Forbes Sect
  */

  add_action('widgets_init', 'pinno_load_home_red_forbes_widget');

  function pinno_load_home_red_forbes_widget() {
    register_widget('pinno_red_forbes_sect_widget');
  }

  class pinno_red_forbes_sect_widget extends WP_Widget { 
    public function __construct() {
        $widget_ops = array( 
          'classname' => 'pinno_red_forbes_sect_widget',
          'description' => 'A widget that displays the fred forbes section posts',
        );
        parent::__construct( 'pinno_red_forbes_sect_widget', 'Home Red Forbes Widget', $widget_ops);
    }

    public function widget( $args, $instance ) { ?>
      <section id="section_red" class="mainsection mainsection--red">

        <div class="channel--header">
            <h2 class="mainsection__title">
              <a href="/tag/red-forbes/" class="f4_module_link">
                <span class="pinno-widget-home-title"> Columnistas </span> 
              </a>
            </h2>
        </div>
        
        <div class="f4_red-forbes__table">
          <div class="f4_mainsection__wrap">
            <div id="red_forbes" class="f4_red-forbes">
                <?php
                $red_forbes_args = array(

                    /*'no_found_rows' => true,
                    'update_post_meta_cache' => false,
                    'update_post_term_cache' => false,
                    'ignore_sticky_posts' => true,*/

                    'category_name' => 'columnas',
                    'post_type' => 'any',
                    'posts_per_page' => 5,


                );

                $red_forbes_articles = new WP_Query($red_forbes_args);

                if ($red_forbes_articles ->have_posts()) :
                    while ($red_forbes_articles ->have_posts()) : $red_forbes_articles ->the_post(); ?>

                        <article class="f4_red-forbes__article">
                            <figure class="f4_red-forbes__article__photo">
                                <?php get_the_avatar(get_the_author_meta('ID')); ?>
                            </figure>
                                <h4 class="channel--article-style_title">                            
                                <a href="<?php the_permalink(); ?>"><?php the_title( );?></a>
                                </h4>
                            
                            <span class="f4_red-forbes__article__author">
                                <?php the_author_posts_link(); ?>
                            </span>
                        </article>

                        <?php
                    endwhile;
                endif;
                wp_reset_postdata(); ?>

            </div>
        </div>
         
        </div>
      </section>
   <?php }


  }