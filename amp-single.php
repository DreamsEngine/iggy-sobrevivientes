<!DOCTYPE html>
<html ⚡ <?php language_attributes(); ?>>
	<head>
		<meta charset="<?php bloginfo('charset'); ?>" >
		<link rel="amphtml" href="<?php echo get_permalink(); ?>amp/" />
    	<meta name="viewport" id="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no" />
		<?php if ( ! function_exists( 'has_site_icon' ) || ! has_site_icon() ) { if(get_option('pinno_favicon')) { ?><link rel="shortcut icon" href="<?php echo esc_url(get_option('pinno_favicon')); ?>" /><?php } } ?>
		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
		<meta property="og:type" content="article" />
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			<?php if (  (function_exists('has_post_thumbnail')) && (has_post_thumbnail())  ) { ?>
				<?php global $post; $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'pinno-post-thumb' ); ?>
				<meta property="og:image" content="<?php echo esc_url( $thumb['0'] ); ?>" />
				<meta name="twitter:image" content="<?php echo esc_url( $thumb['0'] ); ?>" />
			<?php } ?>
			<meta property="og:url" content="<?php the_permalink() ?>" />
			<meta property="og:title" content="<?php the_title_attribute(); ?>" />
			<meta property="og:description" content="<?php echo strip_tags(get_the_excerpt()); ?>" />
			<meta name="twitter:card" content="summary">
			<meta name="twitter:url" content="<?php the_permalink() ?>">
			<meta name="twitter:title" content="<?php the_title_attribute(); ?>">
			<meta name="twitter:description" content="<?php echo strip_tags(get_the_excerpt()); ?>">
		<?php endwhile; endif; ?>
		<script async custom-element="amp-social-share" src="https://cdn.ampproject.org/v0/amp-social-share-0.1.js"></script>
        <script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>
		<script async src="https://cdn.ampproject.org/v0.js"></script>
		<?php do_action( 'amp_post_template_head', $this ); ?>
		<style amp-custom>
			<?php echo file_get_contents( get_template_directory() . '/css/amp-style.css' ); ?>
			<?php echo file_get_contents( get_template_directory() . '/css/amp-media-queries.css' ); ?>
			<?php echo file_get_contents( get_stylesheet_directory() . '/inc/plugins/google/css/amp-style.css' ); ?>
			<?php do_action( 'amp_post_template_css', $this ); ?>
		</style>
	</head>
	<body <?php body_class(''); ?>>
		<?php get_template_part('amp-fly-menu'); ?>
		<div id="pinno-site" class="left relative">
			<div id="pinno-site-wall" class="left relative">
				<div id="pinno-site-main" class="left relative">
					<header id="pinno-main-head-wrap">
						<nav id="pinno-main-nav-wrap" class="left relative">
							<div id="pinno-main-nav-small" class="left relative">
								<div id="pinno-main-nav-small-cont" class="left">
									<div class="pinno-main-box">
										<div id="pinno-nav-small-wrap">
											<div class="pinno-nav-small-cont">
												<div id="pinno-nav-small-left">
													<div class="pinno-fly-but-wrap left relative ampstart-btn caps m2" on="tap:sidebar.toggle" role="button" tabindex="0">
														<span></span>
														<span></span>
														<span></span>
														<span></span>
													</div><!--pinno-fly-but-wrap-->
												</div><!--pinno-nav-small-left-->
												<div class="pinno-nav-small-mid">
													<div class="pinno-nav-small-logo">
														<?php if(get_option('pinno_logo_nav')) { ?>
															<a href="<?php echo esc_url( home_url( '/' ) ); ?>"><amp-img src="<?php echo esc_url(get_option('pinno_logo_nav')); ?>" alt="<?php bloginfo( 'name' ); ?>" data-rjs="2" width="<?php echo esc_html(get_option('pinno_amp_logo')); ?>" height="30" layout="fixed"></amp-img></a>
														<?php } else { ?>
															<a href="<?php echo esc_url( home_url( '/' ) ); ?>"><amp-img src="<?php echo get_template_directory_uri(); ?>/images/logos/logo-nav.png" alt="<?php bloginfo( 'name' ); ?>" data-rjs="2" width="<?php echo esc_html(get_option('pinno_amp_logo')); ?>" height="30" layout="fixed"></amp-img></a>
														<?php } ?>
														<h2 class="pinno-logo-title"><?php bloginfo( 'name' ); ?></h2>
													</div><!--pinno-nav-small-logo-->
												</div><!--pinno-nav-small-mid-->
											</div><!--pinno-nav-small-cont-->
										</div><!--pinno-nav-small-wrap-->
									</div><!--pinno-main-box-->
								</div><!--pinno-main-nav-small-cont-->
							</div><!--pinno-main-nav-small-->
						</nav><!--pinno-main-nav-wrap-->
						<div id="pinno-soc-mob-wrap" class="left relative">
							<div class="pinno-main-box">
								<div class="pinno-soc-mob-cont">
									<amp-social-share class="rounded" type="email" width="36" height="36"></amp-social-share>
									<?php $pinno_amp_fb = get_option('pinno_amp_fb'); if ($pinno_amp_fb) { ?>
										<amp-social-share class="rounded" type="facebook" width="36" height="36" data-param-app_id="<?php echo esc_html($pinno_amp_fb); ?>"></amp-social-share>
									<?php } ?>
									<amp-social-share class="rounded" type="twitter" width="36" height="36"></amp-social-share>
									<amp-social-share class="rounded" type="pinterest" data-param-media="<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'pinno-post-thumb' ); echo esc_url($thumb['0']); ?>" width="36" height="36"></amp-social-share>
									<amp-social-share class="rounded" type="whatsapp" width="36" height="36"></amp-social-share>
								</div><!--pinno-soc-mob-cont-->
							</div><!--pinno-main-box-->
						</div><!--pinno-soc-mob-wrap-->
					</header><!--pinno-main-head-wrap-->

					<div id="pinno-main-body-wrap" class="left relative">
						<?php global $author; $userdata = get_userdata($author); ?>
						<article id="pinno-article-wrap" itemscope itemtype="http://schema.org/NewsArticle">
							<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
								<meta itemscope itemprop="mainEntityOfPage"  itemType="https://schema.org/WebPage" itemid="<?php the_permalink(); ?>"/>
								<div id="pinno-article-cont" class="left relative">
									<div class="pinno-main-box">
										<div id="pinno-post-main" class="left relative">
											<header id="pinno-post-head" class="left relative">
											<amp-ad data-slot="/21825384248/mm-default-top-a" width="300" height="50" type="doubleclick" data-multi-size="300x50,320x50"></amp-ad> <!--pinno-gads-amp-->
												<h3 class="pinno-post-cat left relative"><a class="pinno-post-cat-link" href="<?php $category = get_the_category(); $category_id = get_cat_ID( $category[0]->cat_name ); $category_link = get_category_link( $category_id ); echo esc_url( $category_link ); ?>"><span class="pinno-post-cat left"><?php $category = get_the_category(); echo esc_html( $category[0]->cat_name ); ?></span></a></h3>
												<h1 class="pinno-post-title left entry-title" itemprop="headline"><?php the_title(); ?></h1>
												<?php if ( has_excerpt() ) { ?>
													<span class="pinno-post-excerpt left"><?php the_excerpt(); ?></span>
												<?php } ?>
												<?php $author_info = get_option('pinno_author_info'); if ($author_info == "true") { ?>
													<div class="pinno-author-info-wrap left relative">
														<div class="pinno-author-info-thumb left relative">
															<amp-img src="<?php echo esc_url(get_avatar_url( get_the_author_meta('email') )); ?>" width="46" height="46"  layout="responsive"></amp-img>
														</div><!--pinno-author-info-thumb-->
														<div class="pinno-author-info-text left relative">
															<div class="pinno-author-info-date left relative">
																<p><?php esc_html_e( 'Published', 'iggy-type-0' ); ?></p> <span class="pinno-post-date"><?php printf( esc_html__( '%s ago', 'iggy-type-0' ), human_time_diff( get_the_time( 'U' ), current_time( 'timestamp' ) ) ); ?></span> <p><?php esc_html_e( 'on', 'iggy-type-0' ); ?></p> <span class="pinno-post-date updated"><time class="post-date updated" itemprop="datePublished" datetime="<?php the_time('Y-m-d'); ?>"><?php the_time(get_option('date_format')); ?></time></span>
																<meta itemprop="dateModified" content="<?php the_modified_date('Y-m-d g:i a'); ?>"/>
															</div><!--pinno-author-info-date-->
															<div class="pinno-author-info-name left relative" itemprop="author" itemscope itemtype="https://schema.org/Person">
																<p><?php esc_html_e( 'By', 'iggy-type-0' ); ?></p> <span class="author-name vcard fn author" itemprop="name"><?php the_author_posts_link(); ?></span> <?php $authortwit = get_the_author_meta('twitter'); if ( ! empty ( $authortwit ) ) { ?><a href="<?php echo esc_url(the_author_meta('twitter')); ?>" class="pinno-twit-but" target="_blank"><span class="pinno-author-info-twit-but"><i class="fa fa-twitter fa-2"></i></span></a><?php } ?>
															</div><!--pinno-author-info-name-->
														</div><!--pinno-author-info-text-->
													</div><!--pinno-author-info-wrap-->
												<?php } ?>
											</header>

													<div id="pinno-post-content" class="left relative">
														<?php global $post; $pinno_featured_img = get_option('pinno_featured_img'); $pinno_show_hide = get_post_meta($post->ID, "pinno_featured_image", true); if ($pinno_featured_img == "true") { if ($pinno_show_hide !== "hide") { ?>
																<?php if (  (function_exists('has_post_thumbnail')) && (has_post_thumbnail())  ) { ?>
																	<div id="pinno-post-feat-img" class="left relative pinno-post-feat-img-wide2" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
																		<?php $thumb_id = get_post_thumbnail_id(); $pinno_thumb_array = wp_get_attachment_image_src($thumb_id, 'pinno-post-thumb', true); $pinno_thumb_url = $pinno_thumb_array[0]; $pinno_thumb_width = $pinno_thumb_array[1]; $pinno_thumb_height = $pinno_thumb_array[2]; ?>
																		<amp-img src="<?php echo esc_url($pinno_thumb_url) ?>" width="<?php echo esc_html($pinno_thumb_width) ?>" height="<?php echo esc_html($pinno_thumb_height) ?>" layout="responsive"></amp-img>
																		<meta itemprop="url" content="<?php echo esc_url($pinno_thumb_url) ?>">
																		<meta itemprop="width" content="<?php echo esc_html($pinno_thumb_width) ?>">
																		<meta itemprop="height" content="<?php echo esc_html($pinno_thumb_height) ?>">
																	</div><!--pinno-post-feat-img-->
																	<?php global $post; if(get_post_meta($post->ID, "pinno_photo_credit", true)): ?>
																		<span class="pinno-feat-caption"><?php echo wp_kses_post(get_post_meta($post->ID, "pinno_photo_credit", true)); ?></span>
																	<?php endif; ?>
																<?php } ?>
														<?php } else { ?>
															<div class="pinno-post-img-hide" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
																<?php $thumb_id = get_post_thumbnail_id(); $pinno_thumb_array = wp_get_attachment_image_src($thumb_id, 'pinno-post-thumb', true); $pinno_thumb_url = $pinno_thumb_array[0]; $pinno_thumb_width = $pinno_thumb_array[1]; $pinno_thumb_height = $pinno_thumb_array[2]; ?>
																<meta itemprop="url" content="<?php echo esc_url($pinno_thumb_url) ?>">
																<meta itemprop="width" content="<?php echo esc_html($pinno_thumb_width) ?>">
																<meta itemprop="height" content="<?php echo esc_html($pinno_thumb_height) ?>">
															</div><!--pinno-post-img-hide-->
														<?php } ?>
													<?php } else { ?>
														<div class="pinno-post-img-hide" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
															<?php $thumb_id = get_post_thumbnail_id(); $pinno_thumb_array = wp_get_attachment_image_src($thumb_id, 'pinno-post-thumb', true); $pinno_thumb_url = $pinno_thumb_array[0]; $pinno_thumb_width = $pinno_thumb_array[1]; $pinno_thumb_height = $pinno_thumb_array[2]; ?>
															<meta itemprop="url" content="<?php echo esc_url($pinno_thumb_url) ?>">
															<meta itemprop="width" content="<?php echo esc_html($pinno_thumb_width) ?>">
															<meta itemprop="height" content="<?php echo esc_html($pinno_thumb_height) ?>">
														</div><!--pinno-post-img-hide-->
													<?php } ?>	
													<div id="pinno-content-wrap" class="left relative">
																<div id="pinno-content-body" class="left relative">
																	<div id="pinno-content-body-top" class="left relative">
																		<div id="pinno-content-main" class="left relative">
																			<?php echo $this->get( 'post_amp_content' ); ?>
																			<?php wp_link_pages(); ?>
																		</div><!--pinno-content-main-->
																		<div id="pinno-content-bot" class="left">
																			<div class="pinno-post-tags">
																				<span class="pinno-post-tags-header"><?php esc_html_e( 'Related Topics:', 'iggy-type-0' ); ?></span><span itemprop="keywords"><?php the_tags('','','') ?></span>
																		</div><!--pinno-post-tags-->
																		<div class="pinno-org-wrap" itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
																			<div class="pinno-org-logo" itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
																				<?php if(get_option('pinno_logo_nav')) { ?>
																					<amp-img src="<?php echo esc_url(get_option('pinno_logo_nav')); ?>" alt="<?php bloginfo( 'name' ); ?>" data-rjs="2" width="<?php echo esc_html(get_option('pinno_amp_logo')); ?>" height="30" layout="fixed"></amp-img>
																					<meta itemprop="url" content="<?php echo esc_url(get_option('pinno_logo_nav')); ?>">
																				<?php } else { ?>
																					<amp-img src="<?php echo get_template_directory_uri(); ?>/images/logos/logo-nav.png" alt="<?php bloginfo( 'name' ); ?>" data-rjs="2" width="<?php echo esc_html(get_option('pinno_amp_logo')); ?>" height="30" layout="fixed"></amp-img>
																					<meta itemprop="url" content="<?php echo get_template_directory_uri(); ?>/images/logos/logo-nav.png">
																				<?php } ?>
																			</div><!--pinno-org-logo-->
																			<meta itemprop="name" content="<?php bloginfo( 'name' ); ?>">
																		</div><!--pinno-org-wrap-->
																	</div><!--pinno-content-bot-->
																</div><!--pinno-content-body-top-->

															</div><!--pinno-content-body-->
												</div><!--pinno-content-wrap-->
													<?php if ( comments_open() ) { ?>
														<?php $disqus_id = get_option('pinno_disqus_id'); if ($disqus_id) { if (isset($disqus_id)) {  ?>
															<a href="<?php the_permalink() ?>#pinno-comments-button">
															<div id="pinno-comments-button" class="left relative">
																<span class="pinno-comment-but-text"><?php comments_number(__( 'Comments', 'iggy-type-0'), esc_html__('Comments', 'iggy-type-0'), esc_html__('Comments', 'iggy-type-0')); ?></span>
															</div><!--pinno-comments-button-->
															</a>
														<?php } } else { ?>
															<a href="<?php the_permalink() ?>#pinno-comments-button">
															<div id="pinno-comments-button" class="left relative">
																<span class="pinno-comment-but-text"><?php comments_number(__( 'Click to comment', 'iggy-type-0'), esc_html__('1 Comment', 'iggy-type-0'), esc_html__('% Comments', 'iggy-type-0'));?></span>
															</div><!--pinno-comments-button-->
															</a>
														<?php } ?>
													<?php } ?>
											</div><!--pinno-post-content-->

								</div><!--pinno-post-main-->
									<?php $pinno_trend_posts = get_option('pinno_trend_posts'); if ($pinno_trend_posts == "true") { ?>
										<div id="pinno-post-more-wrap" class="left relative">
											<h4 class="pinno-widget-home-title">
												<span class="pinno-widget-home-title"><?php echo esc_html(get_option('pinno_pop_head')); ?></span>
											</h4>
											<ul class="pinno-post-more-list left relative">
												<?php global $post; $pop_days = esc_html(get_option('pinno_pop_days')); $popular_days_ago = "$pop_days days ago"; $recent = new WP_Query(array('posts_per_page' => '6', 'ignore_sticky_posts'=> 1, 'post__not_in' => array( $post->ID ), 'orderby' => 'meta_value_num', 'order' => 'DESC', 'meta_key' => 'post_views_count', 'date_query' => array( array( 'after' => $popular_days_ago )) )); while($recent->have_posts()) : $recent->the_post(); ?>
							
													<li>
														<?php if (  (function_exists('has_post_thumbnail')) && (has_post_thumbnail())  ) { ?>
															<div class="pinno-post-more-img left relative">
																<a href="<?php the_permalink(); ?>" rel="bookmark">
																<?php $thumb_id = get_post_thumbnail_id(); $pinno_thumb_array = wp_get_attachment_image_src($thumb_id, 'pinno-mid-thumb', true); $pinno_thumb_url = $pinno_thumb_array[0]; $pinno_thumb_width = $pinno_thumb_array[1]; $pinno_thumb_height = $pinno_thumb_array[2]; ?>
																<amp-img class="pinno-reg-img" src="<?php echo esc_url($pinno_thumb_url) ?>" width="<?php echo esc_html($pinno_thumb_width) ?>" height="<?php echo esc_html($pinno_thumb_height) ?>" layout="responsive"></amp-img>
																<?php $thumb_id = get_post_thumbnail_id(); $pinno_thumb_array = wp_get_attachment_image_src($thumb_id, 'pinno-small-thumb', true); $pinno_thumb_url = $pinno_thumb_array[0]; $pinno_thumb_width = $pinno_thumb_array[1]; $pinno_thumb_height = $pinno_thumb_array[2]; ?>
																<amp-img class="pinno-mob-img" src="<?php echo esc_url($pinno_thumb_url) ?>" width="<?php echo esc_html($pinno_thumb_width) ?>" height="<?php echo esc_html($pinno_thumb_height) ?>" layout="responsive"></amp-img>
																</a>
															</div><!--pinno-post-more-img-->
														<?php } ?>
														<div class="pinno-post-more-text left relative">
															<a href="<?php the_permalink(); ?>" rel="bookmark">
															<div class="pinno-cat-date-wrap left relative">
																<span class="pinno-cd-cat left relative"><?php $category = get_the_category(); echo esc_html( $category[0]->cat_name ); ?></span><span class="pinno-cd-date left relative"><?php printf( esc_html__( '%s ago', 'iggy-type-0' ), human_time_diff( get_the_time( 'U' ), current_time( 'timestamp' ) ) ); ?></span>
															</div><!--pinno-cat-date-wrap-->
															<p><?php the_title(); ?></p>
															</a>
														</div><!--pinno-post-more-text-->
													</li>
												<?php endwhile; wp_reset_postdata(); ?>
											</ul>
										</div><!--pinno-post-more-wrap-->
									<?php } ?>
								</div><!--pinno-main-box-->
							</div><!--pinno-article-cont-->
							<?php setCrunchifyPostViews(get_the_ID()); ?>
						<?php endwhile; endif; ?>
					</article><!--pinno-article-wrap-->	
				</div><!--pinno-main-body-wrap-->
				<footer id="pinno-foot-wrap" class="left relative">
					<div id="pinno-foot-bot" class="left relative">
						<div class="pinno-main-box">
							<div id="pinno-foot-copy" class="left relative">
								<p><?php echo wp_kses_post(get_option('pinno_copyright')); ?></p>
							</div><!--pinno-foot-copy-->
						</div><!--pinno-main-box-->
					</div><!--pinno-foot-bot-->
				</footer>
			</div><!--pinno-site-main-->
		</div><!--pinno-site-wall-->
	</div><!--pinno-site-->
	<?php do_action( 'amp_post_template_footer', $this ); ?>
</body>
</html>