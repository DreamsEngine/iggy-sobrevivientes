const sidebarMenuHome = () => {
  const popularBtn = document.getElementById("pick-popular");
  const pickBtn = document.getElementById("pick-editors");
  const popularTab = document.getElementById("popular-tab");
  const editorsTab = document.getElementById("editors-tab");

  if (popularBtn) {
    pickBtn.addEventListener("click", () => {
      popularBtn.classList.remove("pop-picks__nav-btn--active");
      pickBtn.classList.add("pop-picks__nav-btn--active");
      popularTab.classList.remove("show");
      editorsTab.classList.add("show");
    });

    popularBtn.addEventListener("click", () => {
      pickBtn.classList.remove("pop-picks__nav-btn--active");
      popularBtn.classList.add("pop-picks__nav-btn--active");
      editorsTab.classList.remove("show");
      popularTab.classList.add("show");
    });
  }

  const getIdYoutube = url => {
    const regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#]*).*/;
    const match = url.match(regExp);

    return match && match[7].length === 11 ? match[7] : false;
  };

  const showHomeAsideVideo = () => {
    const videoBtn = document.getElementById("homeVideoBtn");

    if (document.body.contains(videoBtn)) {
      videoBtn.addEventListener("click", () => {
        window.DreamsBlockeshowModuleOneType5VideodRefresh = true;
        const holder = document.querySelector(".home-aside-video");
        const videoUrl = videoBtn.getAttribute("data-videourl");
        const idVideo = getIdYoutube(videoUrl);
        const iframe = document.createElement("iframe");
        iframe.src = `https://www.youtube.com/embed/${idVideo.replace(
          / /g,
          ""
        )}?enablejsapi=1&autoplay=1`;
        iframe.setAttribute(
          "allow",
          "accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture;"
        );
        holder.innerHTML = iframe.outerHTML;
      });
    }
  };

  showHomeAsideVideo();
};

export default sidebarMenuHome;
